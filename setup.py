#!/usr/bin/env python

"""The setup script."""

from setuptools import find_packages, setup

import pyesmda

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("CHANGELOG.rst") as history_file:
    history = history_file.read()

requirements = ["numpy>=1.21"]

setup_requirements = [
    "pytest-runner",
    "wheel",
]

test_requirements = [
    "pytest>=3",
]

setup(
    author=pyesmda.__author__,
    author_email="antoine.collet5@gmail.com",
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
    ],
    description="Python Ensemble Smoother with Multiple Data Assimilation",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords=[
        "esmda",
        "es-mda",
        "inversion",
        "inverse problem",
        "parameter estimation",
        "stochastic-optimization",
        "ensemble smoother",
    ],
    name="pyesmda",
    packages=find_packages(include=["pyesmda", "pyesmda.*"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/antoinecollet5/pyesmda",
    version=pyesmda.__version__,
    zip_safe=False,
)
